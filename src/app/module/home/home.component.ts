import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  clicked: boolean = false;
  is_submitted: boolean = false;

  contactForm = new FormGroup({
    message: new FormControl('', [
      Validators.required,
    ]),
  });


  ngOnInit() {
    window.onscroll = function () { scrollFunction() };

    function scrollFunction() {
      if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50){
        document.getElementById("mynav").style.background= "rgba(0,0,0,0.789)";


      } else {
        document.getElementById("mynav").style.background= "";

      }
    }
  }
  get contactControl() {
    return this.contactForm.controls;
  }

  query() {
    this.is_submitted = true;
  }
}
